package csbi

import (
	"html/template"

	"github.com/openconfig/ygot/ygen"
)

var pluginStruct = ygen.GoStructCodeSnippet{
	StructName: "Csbi",
	StructDef: `type Csbi struct {
	schema *ytypes.Schema
	id     uuid.UUID
}`,
	Methods: `
func (sbi *Csbi) SbiIdentifier() string {
	return "plugin sbi"
}

func (sbi *Csbi) SetNode(schema *yang.Entry, root interface{}, path *gpb.Path, val interface{}, opts ...ytypes.SetNodeOpt) error {
	log.WithFields(log.Fields{
		"schema": schema.Name,
		"path":   path,
		"val":    val,
		"opts":   opts,
	}).Trace("entering SetNode()")
	return ytypes.SetNode(schema, root.(*Device), path, val, opts...)
}

func (sbi *Csbi) Schema() *ytypes.Schema {
	schema, err := Schema()
	sbi.schema = schema
	if err != nil {
		log.Fatal(err)
	}
	return schema
}

func (sbi *Csbi) ID() uuid.UUID {
	return sbi.id
}

func (sbi *Csbi) Type() spb.Type {
	return spb.Type_CONTAINERISED
}

func (sbi *Csbi) Unmarshal(bytes []byte, fields []string, goStruct ygot.ValidatedGoStruct, opt ...ytypes.UnmarshalOpt) error {
	log.WithFields(log.Fields{}).Trace("entering Unmarshal()")
	return unmarshal(sbi.Schema(), bytes, fields, goStruct, opt...)
}

// unmarshal parses gNMI response to a go struct.
func unmarshal(schema *ytypes.Schema, bytes []byte, fields []string, goStruct ygot.ValidatedGoStruct, opt ...ytypes.UnmarshalOpt) error {
	defer func() {
		if r := recover(); r != nil {
			log.Error(r.(error))
		}
	}()
	log.WithFields(log.Fields{
		"schema": schema.RootSchema().Name,
		"fields": fields,
		"bytes":  len(bytes),
		"opts":   opt,
	}).Trace("entering unmarshal()")

	// Load SBI definition
	root, err := ygot.DeepCopy(schema.Root)
	if err != nil {
		return err
	}
	validatedDeepCopy, ok := root.(ygot.ValidatedGoStruct)
	if !ok {
		return fmt.Errorf("no validated go struct")
	}
	ygot.BuildEmptyTree(validatedDeepCopy)
	log.Trace("built empty tree")

	var fieldStruct ygot.ValidatedGoStruct
	if len(fields) != 0 {
		log.Trace("fetching fields")
		fieldStruct, err = getField(validatedDeepCopy, fields)
		if err != nil {
			return err
		}
	} else {
		log.Trace("using root struct")
		fieldStruct = validatedDeepCopy
	}

	if err := Unmarshal(bytes, fieldStruct, opt...); err != nil {
		return err
	}
	ygot.PruneEmptyBranches(validatedDeepCopy)
	log.Trace("pruned empty branches")
	log.Trace("merging structs...")
	return ygot.MergeStructInto(goStruct, validatedDeepCopy)
}

// getField traverses the GoStruct and returns the field that represents the
// tail of the path
func getField(inStruct ygot.ValidatedGoStruct, fields []string) (ygot.ValidatedGoStruct, error) {
	defer func() {
		if r := recover(); r != nil {
			log.Error(r.(error))
		}
	}()
	log.WithFields(log.Fields{
		"fields": fields,
	}).Trace("getting field")
	f := fields[0]
	log.Tracef("field name: %v", f)
	s := reflect.ValueOf(inStruct)
	h := reflect.Indirect(s).FieldByName(f).Interface()
	outStruct, ok := h.(ygot.ValidatedGoStruct)
	if !ok {
		t := reflect.TypeOf(h)
		if !(util.IsTypeStruct(t) || util.IsTypeStructPtr(t)) {
			return nil, fmt.Errorf("cannot process entry of type %v, request longer or shorter path", t)
		}
		return nil, fmt.Errorf("expected ValidatedGoStruct got %v", t)
	}
	if len(fields) > 1 {
		log.Trace("fetching fields")
		return getField(outStruct, fields[1:])
	}
	return outStruct, nil
}`,
}

const pluginImportAmendmend = `

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	"github.com/google/uuid"
	"github.com/openconfig/ygot/util"
	log "github.com/sirupsen/logrus"
)

var PluginSymbol Csbi
`

var templater *template.Template

const pluginGoModTemplate = `module << .ModuleName >>

go << .GoVersion >>

require (<<range $element := .Dependencies>>
	<<$element.Name>> v<<$element.Version>>
<<end>>)
`

// Module represents a Go module entry in go.mod. Name and version needed.
type Module struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

// GoMod represents a go.mod file used for templates.
type GoMod struct {
	ModuleName   string   `json:"name"`
	GoVersion    string   `json:"go_version"`
	Dependencies []Module `json:"dependencies"`
}
