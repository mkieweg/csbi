module code.fbi.h-da.de/danet/plugin-autogen

go 1.16

require (
	code.fbi.h-da.de/danet/api v0.2.5-0.20210710121641-95bb981d8c97
	github.com/google/uuid v1.2.0
	github.com/openconfig/gnmi v0.0.0-20210707145734-c69a5df04b53
	github.com/openconfig/goyang v0.2.7
	github.com/openconfig/ygot v0.11.2
	github.com/sirupsen/logrus v1.8.1
)
