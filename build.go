package csbi

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"path/filepath"
	"time"

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/archive"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
)

// nolint
type ErrorLine struct {
	Error       string      `json:"error"`
	ErrorDetail ErrorDetail `json:"errorDetail"`
}

// nolint
type ErrorDetail struct {
	Message string `json:"message"`
}

func buildImage(d Deployment, dockerClient *client.Client) error {
	labels := prometheus.Labels{"type": spb.Type_CONTAINERISED.String()}
	start := promStartHook(labels, buildsTotal)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*300)
	defer cancel()

	p := filepath.Join(d.ID.String(), "/")
	tar, err := archive.TarWithOptions(p, &archive.TarOptions{})
	if err != nil {
		return err
	}

	opts := types.ImageBuildOptions{
		Dockerfile: "Dockerfile",
		Tags:       []string{d.Name},
		Remove:     true,
	}
	res, err := dockerClient.ImageBuild(ctx, tar, opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	err = print(res.Body)
	if err != nil {
		return err
	}
	promEndHook(labels, start, buildDurationSecondsTotal, buildDurationSeconds)
	return nil
}

func print(rd io.Reader) error {
	var lastLine string

	scanner := bufio.NewScanner(rd)
	for scanner.Scan() {
		lastLine = scanner.Text()
		fmt.Println(scanner.Text())
	}

	errLine := &ErrorLine{}
	json.Unmarshal([]byte(lastLine), errLine)
	if errLine.Error != "" {
		return errors.New(errLine.Error)
	}

	return scanner.Err()
}

func buildPlugin(id string) error {
	labels := prometheus.Labels{"type": spb.Type_PLUGIN.String()}
	start := promStartHook(labels, buildsTotal)

	var stderr bytes.Buffer
	buildDir := id
	goModDownload := exec.Command("go", "mod", "tidy")
	goModDownload.Dir = buildDir
	goModDownload.Stderr = &stderr
	err := goModDownload.Run()
	if err != nil {
		log.Error(stderr.String())
		return err
	}
	stderr.Reset()
	buildCommand := []string{
		"go",
		"build",
		"-buildmode=plugin",
		"-o",
		"./plugin.so",
		"./gostructs.go",
	}

	cmd := exec.Command(buildCommand[0], buildCommand[1:]...)
	cmd.Dir = buildDir
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		log.Error(stderr.String())
		return err
	}
	promEndHook(labels, start, buildDurationSecondsTotal, buildDurationSeconds)
	return nil
}
