package csbi

import (
	"context"
	"reflect"
	"testing"

	tpb "code.fbi.h-da.de/danet/api/go/gosdn/transport"
	"github.com/openconfig/gnmi/proto/gnmi"
)

func TestDiscover(t *testing.T) {
	type args struct {
		ctx  context.Context
		opts *tpb.TransportOption
	}
	tests := []struct {
		name    string
		args    args
		want    []*gnmi.ModelData
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Discover(tt.args.ctx, tt.args.opts)
			if (err != nil) != tt.wantErr {
				t.Errorf("Discover() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Discover() = %v, want %v", got, tt.want)
			}
		})
	}
}
