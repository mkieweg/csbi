/*
Copyright © 2021 Manuel Kieweg <mail@manuelkieweg.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

package cmd

import (
	"context"
	"net"

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	"code.fbi.h-da.de/danet/csbi"
	"code.fbi.h-da.de/danet/csbi/config"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc/peer"
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "generates a blank csbi boilerplate",

	Run: func(cmd *cobra.Command, args []string) {
		var t spb.Type
		switch opcode {
		case "plugin":
			t = spb.Type_PLUGIN
		case "csbi":
			t = spb.Type_CONTAINERISED
		default:
			log.Fatal("invalid opcode")
		}
		addr, err := net.ResolveTCPAddr("tcp", "localhost:55055")
		if err != nil {
			log.Fatal(err)
		}
		repo := csbi.NewRepository(config.RepositoryBasePath())
		ctx := peer.NewContext(context.Background(), &peer.Peer{Addr: addr})
		_, err = csbi.Generate(ctx, nil, repo, t)
		if err != nil {
			log.Error(err)
		}
	},
}

var opcode string

func init() {
	rootCmd.AddCommand(generateCmd)
	generateCmd.Flags().StringVar(&opcode, "type", "plugin", "generation target (csbi or plugin)")
}
