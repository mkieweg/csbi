package main

import (
	"context"
	"fmt"
	"net"

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	"code.fbi.h-da.de/danet/csbi"
	"code.fbi.h-da.de/danet/csbi/config"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"google.golang.org/grpc/peer"
)

func init() {
	log.SetLevel(log.DebugLevel)
	viper.SetConfigName(".csbi")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(fmt.Errorf("error reading config: %w", err))
	}
	log.WithFields(viper.AllSettings()).Debug("current viper config")
}

func main() {
	repo := csbi.NewRepository(config.RepositoryBasePath())
	p := &peer.Peer{
		Addr: &net.IPAddr{IP: net.IPv4zero},
	}
	_, err := csbi.Generate(peer.NewContext(context.Background(), p), nil, repo, spb.Type_CONTAINERISED)
	if err != nil {
		log.Fatal(err)
	}
}
