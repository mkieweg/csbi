package csbi

import (
	"io/fs"
	"reflect"
	"testing"

	gpb "github.com/openconfig/gnmi/proto/gnmi"
)

func TestNewRepository(t *testing.T) {
	type args struct {
		basePath string
	}
	tests := []struct {
		name string
		args args
		want Repository
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRepository(tt.args.basePath); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repo_YANGPathsWithSuffix(t *testing.T) {
	type fields struct {
		fs Filesystem
	}
	tests := []struct {
		name    string
		fields  fields
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo{
				fs: tt.fields.fs,
			}
			got, err := r.YANGPathsWithSuffix()
			if (err != nil) != tt.wantErr {
				t.Errorf("repo.YANGPathsWithSuffix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("repo.YANGPathsWithSuffix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repo_FindYANGFiles(t *testing.T) {
	type fields struct {
		fs Filesystem
	}
	type args struct {
		models []*gpb.ModelData
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []string
		want1  []error
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo{
				fs: tt.fields.fs,
			}
			got, got1 := r.FindYANGFiles(tt.args.models)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("repo.FindYANGFiles() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("repo.FindYANGFiles() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_filesystem_Open(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    fs.File
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			got, err := osfs.Open(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.Open() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("filesystem.Open() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_filesystem_ReadDir(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []fs.DirEntry
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			got, err := osfs.ReadDir(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.ReadDir() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("filesystem.ReadDir() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_filesystem_ReadFile(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			got, err := osfs.ReadFile(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.ReadFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("filesystem.ReadFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_filesystem_Stat(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    fs.FileInfo
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			got, err := osfs.Stat(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.Stat() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("filesystem.Stat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_filesystem_Glob(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		pattern string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantPaths []string
		wantErr   bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			gotPaths, err := osfs.Glob(tt.args.pattern)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.Glob() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPaths, tt.wantPaths) {
				t.Errorf("filesystem.Glob() = %v, want %v", gotPaths, tt.wantPaths)
			}
		})
	}
}

func Test_filesystem_fsValid(t *testing.T) {
	type fields struct {
		root string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid",
			fields: fields{
				root: "testdata",
			},
			args: args{
				name: "models/release",
			},
			wantErr: false,
		},
		{
			name: "invalid",
			fields: fields{
				root: "",
			},
			args: args{
				name: "",
			},
			wantErr: true,
		},
		{
			name: "invalid root",
			fields: fields{
				root: "/",
			},
			args: args{
				name: "",
			},
			wantErr: true,
		},
		{
			name: "invalid tail",
			fields: fields{
				root: "",
			},
			args: args{
				name: "/",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osfs := &filesystem{
				root: tt.fields.root,
			}
			err := osfs.fsValid(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("filesystem.fsValid() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_findBestMatch(t *testing.T) {
	type args struct {
		paths   []string
		org     string
		version string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "default",
			args: args{
				paths: []string{
					"/openconfig/interfaces",
					"/ietf/interfaces",
					"/arista/interfaces",
				},
				org: "openconfig",
			},
			want: "/openconfig/interfaces",
		},
		{
			name: "take first",
			args: args{
				paths: []string{
					"/openconfig/interfaces",
					"/openconfig/acl",
					"/openconfig/abc",
				},
				org: "openconfig",
			},
			want: "/openconfig/interfaces",
		},
		{
			name: "no org",
			args: args{
				paths: []string{
					"/openconfig/interfaces",
					"/openconfig/acl",
					"/openconfig/abc",
				},
				org: "",
			},
			want: "/openconfig/interfaces",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := findBestMatch(tt.args.paths, tt.args.org, tt.args.version)
			if (err != nil) != tt.wantErr {
				t.Errorf("findBestMatch() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("findBestMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_searchYANGFile(t *testing.T) {
	type args struct {
		fsys    Filesystem
		name    string
		org     string
		version string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := searchYANGFile(tt.args.fsys, tt.args.name, tt.args.org, tt.args.version)
			if (err != nil) != tt.wantErr {
				t.Errorf("searchYANGFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("searchYANGFile() = %v, want %v", got, tt.want)
			}
		})
	}
}
