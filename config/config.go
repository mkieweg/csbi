package config

import (
	"time"

	"github.com/spf13/viper"
)

// RepositoryBasePath returns the repository base path from viper
func RepositoryBasePath() string {
	return viper.GetString("repository-base-path")
}

// RepositoryAccessToken returns the repository access token from viper
func RepositoryAccessToken() string {
	return viper.GetString("repository-access-token")
}

// OrchestratorShutdownTimeout returns the orchestrator shutdown timer from viper
func OrchestratorShutdownTimeout() time.Duration {
	return viper.GetDuration("orchestrator-shutown-timeout")
}

// DockerOrchestratorNetwork returns the docker orchestrator network from viper
func DockerOrchestratorNetwork() string {
	return viper.GetString("docker-orchestrator-network")
}
