package csbi

import (
	"fmt"
	"io"
	"plugin"
	"testing"

	"code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	"github.com/docker/docker/client"
	"github.com/google/uuid"
)

func Test_buildImage(t *testing.T) {
	type args struct {
		d            Deployment
		dockerClient *client.Client
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := buildImage(tt.args.d, tt.args.dockerClient); (err != nil) != tt.wantErr {
				t.Errorf("buildImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_print(t *testing.T) {
	type args struct {
		rd io.Reader
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := print(tt.args.rd); (err != nil) != tt.wantErr {
				t.Errorf("print() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_buildPlugin(t *testing.T) {
	t.Skip("skipped due to package version inconsistencies")
	type args struct {
		d Deployment
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "default",
			args: args{
				d: Deployment{
					ID: uuid.Nil,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := buildPlugin("testdata/00000000-0000-0000-0000-000000000000"); (err != nil) != tt.wantErr {
				t.Errorf("buildPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err := loadPlugin(tt.args.d.ID.String()); (err != nil) != tt.wantErr {
				t.Errorf("buildPlugin() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func loadPlugin(path string) error {
	p, err := plugin.Open("testdata/00000000-0000-0000-0000-000000000000/plugin.so")
	if err != nil {
		return err
	}

	symbol, err := p.Lookup("PluginSymbol")
	if err != nil {
		return err
	}

	_, ok := symbol.(southbound.SouthboundInterface)
	if !ok {
		return fmt.Errorf("invalid plugin type")
	}
	return nil
}
