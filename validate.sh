#!/bin/zsh
rm pyang.log
for line in "${(@f)"$(<./yangpaths.txt)"}"
{
  pyang --plugindir ~/yang/oc-pyang/openconfig_pyang/plugins --oc-only ${line} -p ./models &>> pyang.log
}