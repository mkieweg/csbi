package csbi

import (
	"context"
	"testing"

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	"github.com/openconfig/ygot/ygen"
	"google.golang.org/grpc/peer"
)

func Test_write(t *testing.T) {
	type args struct {
		code    *ygen.GeneratedGoCode
		path    string
		sbiType spb.Type
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := write(context.Background(), tt.args.code, tt.args.path, tt.args.sbiType); (err != nil) != tt.wantErr {
				t.Errorf("write() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_writeCsbi(t *testing.T) {
	type args struct {
		code *ygen.GeneratedGoCode
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := writeCsbi(peer.NewContext(context.Background(), &peer.Peer{}), tt.args.code, tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("writeCsbi() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_writePlugin(t *testing.T) {
	type args struct {
		code *ygen.GeneratedGoCode
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := writePlugin(tt.args.code, tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("writePlugin() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_copyFile(t *testing.T) {
	type args struct {
		path     string
		filename string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := copyFile(tt.args.path, tt.args.filename); (err != nil) != tt.wantErr {
				t.Errorf("copyFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_writeCode(t *testing.T) {
	type args struct {
		path string
		code *ygen.GeneratedGoCode
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := writeCode(tt.args.path, tt.args.code); (err != nil) != tt.wantErr {
				t.Errorf("writeCode() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_writeGoMod(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := writeGoMod(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("writeGoMod() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
