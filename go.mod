module code.fbi.h-da.de/danet/csbi

go 1.16

require (
	code.fbi.h-da.de/danet/api v0.2.5-0.20210710121641-95bb981d8c97
	code.fbi.h-da.de/danet/gosdn v0.0.3-0.20210710180612-63c55de5d87e
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/Microsoft/hcsshim v0.8.20 // indirect
	github.com/containerd/containerd v1.5.2 // indirect
	github.com/docker/docker v20.10.7+incompatible // as per https://github.com/moby/moby/issues/41191#issuecomment-656342401
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/google/uuid v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/moby/sys/mount v0.2.0 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/openconfig/gnmi v0.0.0-20210707145734-c69a5df04b53
	github.com/openconfig/goyang v0.2.9
	github.com/openconfig/ygot v0.12.0
	github.com/opencontainers/runc v1.0.0 // indirect
	github.com/prometheus/client_golang v1.9.0
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.18.0
	github.com/prometheus/prom2json v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	google.golang.org/grpc v1.39.0
)
