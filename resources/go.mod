module code.fbi.h-da.de/danet/csbi-autogen

go 1.16

require (
	code.fbi.h-da.de/danet/api v0.2.5-0.20210722102157-e7e463162450
	code.fbi.h-da.de/danet/forks/goarista v0.0.0-20210709163519-47ee8958ef40
	code.fbi.h-da.de/danet/gosdn v0.0.3-0.20210922181718-b090f39fc0c8
	github.com/google/gnxi v0.0.0-20210423111716-4b504ef806a7
	github.com/google/uuid v1.2.0
	github.com/openconfig/gnmi v0.0.0-20210707145734-c69a5df04b53
	github.com/openconfig/goyang v0.2.9
	github.com/openconfig/ygot v0.12.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.39.0
)
