# syntax = docker/dockerfile:1.2

FROM golang:1.16-alpine AS installer
WORKDIR /build
RUN apk add --no-cache git make build-base
RUN apk add --update --no-cache alpine-sdk
COPY go.mod .
COPY go.sum .
RUN go mod download

FROM installer as builder
COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build \
GOOS=linux go build -o orchestrator ./cmd/csbi/main.go

FROM golang:1.16-alpine
RUN apk add --no-cache git make build-base
RUN apk add --update --no-cache alpine-sdk
COPY --from=builder /build/orchestrator .
COPY --from=builder /build/models ./models
COPY --from=builder /build/resources ./resources
EXPOSE 55056
ENTRYPOINT [ "./orchestrator" ]
CMD [""]