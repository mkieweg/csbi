package csbi

import gpb "github.com/openconfig/gnmi/proto/gnmi"

// nolint
const (
	// OpenconfigInterfacesModel is the openconfig YANG model for interfaces.
	OpenconfigInterfacesModel = "openconfig-interfaces"
	// OpenconfigOpenflowModel is the openconfig YANG model for openflow.
	OpenconfigOpenflowModel = "openconfig-openflow"
	// OpenconfigPlatformModel is the openconfig YANG model for platform.
	OpenconfigPlatformModel = "openconfig-platform"
	// OpenconfigSystemModel is the openconfig YANG model for system.
	OpenconfigSystemModel = "openconfig-system"
	// OpenconfigAclModel is the openconfig YANG model for acl.
	OpenconfigAclModel = "openconfig-acl"
	// OpenconfigAclModel is the openconfig YANG model for network-instance.
	OpenconfigNetworkInstance = "openconfig-network-instance"
)

// nolint
var (
	// ModelData is a list of supported models.
	ModelData = []*gpb.ModelData{{
		Name:         OpenconfigInterfacesModel,
		Organization: "OpenConfig working group",
		Version:      "2.0.0",
	}, {
		Name:         OpenconfigOpenflowModel,
		Organization: "OpenConfig working group",
		Version:      "0.1.0",
	}, {
		Name:         OpenconfigPlatformModel,
		Organization: "OpenConfig working group",
		Version:      "0.5.0",
	}, {
		Name:         OpenconfigSystemModel,
		Organization: "OpenConfig working group",
		Version:      "0.2.0",
	}, {
		Name:         OpenconfigAclModel,
		Organization: "OpenConfig working group",
		Version:      "0.2.0",
	}, {
		Name:         OpenconfigNetworkInstance,
		Organization: "OpenConfig working group",
		Version:      "0.2.0",
	}}
)
