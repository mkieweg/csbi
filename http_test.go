package csbi

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func Test_httpServer(t *testing.T) {
	tests := []struct {
		name     string
		endpoint string
		handler  func(http.ResponseWriter, *http.Request)
		want     *http.Response
		wantErr  bool
	}{
		{
			name:     "livez",
			endpoint: "/livez",
			handler:  healthCheck,
			want:     &http.Response{StatusCode: http.StatusOK},
			wantErr:  false,
		},
		{
			name:     "readyz",
			endpoint: "/readyz",
			handler:  readynessCheck,
			want:     &http.Response{StatusCode: http.StatusOK},
			wantErr:  false,
		},
		{
			name:     "metrics",
			endpoint: "/metrics",
			handler:  promhttp.Handler().ServeHTTP,
			want:     &http.Response{StatusCode: http.StatusOK},
			wantErr:  false,
		},
	}
	startHttpServer()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", tt.endpoint, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(tt.handler)

			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != http.StatusOK {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, http.StatusOK)
			}
		})
	}
}
