package csbi

import (
	"context"
	"reflect"

	spb "code.fbi.h-da.de/danet/api/go/gosdn/southbound"
	tpb "code.fbi.h-da.de/danet/api/go/gosdn/transport"
	"code.fbi.h-da.de/danet/gosdn/nucleus"
	"code.fbi.h-da.de/danet/gosdn/nucleus/errors"
	"github.com/openconfig/gnmi/proto/gnmi"
)

// Discover sends a gnmi Capabilities request to the specified target and
// returns the gnmi.ModelData
func Discover(ctx context.Context, opts *tpb.TransportOption) ([]*gnmi.ModelData, error) {
	sbi := nucleus.NewSBI(spb.Type_OPENCONFIG)
	t, err := nucleus.NewTransport(opts, sbi)
	if err != nil {
		return nil, err
	}
	transport, ok := t.(*nucleus.Gnmi)
	if !ok {
		return nil, &errors.ErrInvalidTypeAssertion{}
	}
	resp, err := transport.Capabilities(ctx)
	if err != nil {
		return nil, err
	}
	capabilities, ok := resp.(*gnmi.CapabilityResponse)
	if !ok {
		return nil, &errors.ErrInvalidTypeAssertion{
			Value: resp,
			Type:  reflect.TypeOf(&gnmi.CapabilityResponse{}),
		}
	}
	return capabilities.SupportedModels, nil
}
