package csbi

import (
	"context"
	"reflect"
	"testing"

	pb "code.fbi.h-da.de/danet/api/go/gosdn/csbi"
)

func Test_server_Get(t *testing.T) {
	type args struct {
		ctx context.Context
		req *pb.GetRequest
	}
	tests := []struct {
		name    string
		s       server
		args    args
		want    *pb.GetResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.Get(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("server.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("server.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_server_Create(t *testing.T) {
	type args struct {
		ctx context.Context
		req *pb.CreateRequest
	}
	tests := []struct {
		name    string
		s       server
		args    args
		want    *pb.CreateResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.Create(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("server.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("server.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_server_CreatePlugin(t *testing.T) {
	type args struct {
		req    *pb.CreateRequest
		stream pb.Csbi_CreatePluginServer
	}
	tests := []struct {
		name    string
		s       server
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.s.CreatePlugin(tt.args.req, tt.args.stream); (err != nil) != tt.wantErr {
				t.Errorf("server.CreatePlugin() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_server_Delete(t *testing.T) {
	type args struct {
		ctx context.Context
		req *pb.DeleteRequest
	}
	tests := []struct {
		name    string
		s       server
		args    args
		want    *pb.DeleteResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.Delete(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("server.Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("server.Delete() = %v, want %v", got, tt.want)
			}
		})
	}
}
