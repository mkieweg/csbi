package csbi

import (
	"context"
	"reflect"
	"testing"

	"github.com/google/uuid"
	gpb "github.com/openconfig/gnmi/proto/gnmi"
)

func TestNewOrchestrator(t *testing.T) {
	type args struct {
		flavour OrchestratorType
	}
	tests := []struct {
		name    string
		args    args
		want    Orchestrator
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewOrchestrator(tt.args.flavour)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewOrchestrator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOrchestrator() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dockerOrchestrator_Build(t *testing.T) {
	type args struct {
		model []*gpb.ModelData
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		want    Deployment
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.o.Build(context.Background(), tt.args.model)
			if (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dockerOrchestrator.Build() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dockerOrchestrator_Deploy(t *testing.T) {
	type args struct {
		deployment Deployment
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.Deploy(tt.args.deployment); (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.Deploy() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_dockerOrchestrator_Destroy(t *testing.T) {
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.Destroy(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.Destroy() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_dockerOrchestrator_Get(t *testing.T) {
	type args struct {
		id uuid.UUID
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		want    Deployment
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.o.Get(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dockerOrchestrator.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dockerOrchestrator_Shutdown(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name string
		o    *dockerOrchestrator
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.o.Shutdown(tt.args.ctx)
		})
	}
}

func Test_dockerOrchestrator_Repository(t *testing.T) {
	tests := []struct {
		name string
		o    *dockerOrchestrator
		want Repository
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.o.Repository(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dockerOrchestrator.Repository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dockerOrchestrator_deploy(t *testing.T) {
	type args struct {
		d Deployment
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		want    Deployment
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.o.deploy(tt.args.d)
			if (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.deploy() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dockerOrchestrator.deploy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dockerOrchestrator_prune(t *testing.T) {
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		o       *dockerOrchestrator
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.prune(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("dockerOrchestrator.prune() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
